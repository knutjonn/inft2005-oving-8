import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// stegdefinisjon for å legge inn betalingsinfo
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    // Antar at jeg har produkter i handlekurven, men åpner nettkiosken
    cy.visit('http://localhost:8080');
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click(); 
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Ola Nordmann'); 
    cy.get('#address').type('Storgata 1'); 
    cy.get('#postCode').type('0123'); /
    cy.get('#city').type('Oslo'); 
    cy.get('#creditCardNo').type('1234567890123456'); 
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('input[type="submit"]').click(); // Bruker submit som inputtype slik det nevnes i side 4 i doku "WEB-TESTING MED CYPRESS.JS", fordi spesifikk ID mangler
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('contain', 'Din ordre er registrert.'); // Basert på innhold i reciept.html
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').type(''); // Tomt navn som ett eksempel
    cy.get('#address').type('');
    cy.get('#postCode').type('ABC'); // Ugyldig postnummer
    cy.get('#city').type('');
    cy.get('#creditCardNo').type('123'); // Ugyldig kortnummer
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('#fullNameError').should('contain', 'Navn er påkrevd');
    cy.get('#addressError').should('contain', 'Adresse er påkrevd');
    cy.get('#postCodeError').should('contain', 'Ugyldig postnummer');
    cy.get('#cityError').should('contain', 'Poststed er påkrevd');
    cy.get('#creditCardNoError').should('contain', 'Ugyldig kortnummer');
});