import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    //Antar at det er en dropdown for produkter og et inputfelt for kvantitet, samt en knapp for å legge til i handlekurven.
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    //Kontrollerer at handlekurven inneholder de lagt til varene
    cy.get('#list li').should('have.length', 4);
});

And(/^den skal ha riktig totalpris$/, function () {
    cy.get('#price').should('have.text', '33');
});

// Steps for deleting items
And(/^lagt inn varer og kvanta$/, () => {
    // This step assumes items have been added as per the previous steps.
});

When(/^jeg sletter varer$/, () => {
    // Assuming there's a delete button next to each item in the cart for deletion.
    // This example clicks the first delete button found.
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    // Verifying the item is no longer in the cart. Adjust as per your actual implementation.
    cy.get('#list li').should('not.exist'); // Or check for specific item absence.
});

// Steps for updating quantities
When(/^jeg oppdaterer kvanta for en vare$/, () => {
    // Assuming a way to update quantity directly in the cart or a re-addition with updated quantity.
    // This step needs specific implementation based on the app's functionality.
    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('3'); // Updating quantity
    cy.get('#saveItem').click(); // Assuming re-adding updates quantity.
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    // Verifying the cart contains the correct quantity. Implementation depends on how the app shows cart item quantities.
    // This example assumes a method to check specific item quantity, which needs adjustment to fit actual application structure.
    cy.get('#list').find('li').contains('Smørbukk').should('contain', '3');
});
